/**

Buddha Quotes
Copyright (C) 2021  BanDev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

package org.bandev.buddhaquotes.core

import android.content.Context

class Theme {

    /**
     * Returns 0 for light mode, 1 for dark mode, 2 for system default
     * @param [context] context of activity (Context)
     * @return 0 for light mode, 1 for dark mode, 2 for system default (Int)
     */

    fun getAppTheme(context: Context): Int {
        val sharedPrefs = context.getSharedPreferences("Settings", 0)
        return sharedPrefs.getInt("appThemeInt", 2)
    }
}